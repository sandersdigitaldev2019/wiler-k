var header = {
    Accept: "application/json",
    "REST-range": "resources=0-10",
    "Content-Type": "application/json; charset=utf-8"
},
insertMasterData = function (t, e, a, i) {
    $.ajax({
        url: "/api/dataentities/" + t + "/documents/",
        type: "PATCH",
        data: a,
        headers: header,
        success: function (t) {
            i(t)
        },
        error: function (t) {}
    })
},
selectMasterData = function (t, e, a, i) {
    $.ajax({
        url: "/api/dataentities/" + t + "/search?" + a,
        type: "GET",
        headers: header,
        success: function (t) {
            i(t)
        },
        error: function (t) {}
    })
};

$mobile = 1025;

var compra_assistida = (function () {
var steps = {
    retira_acentos: function (text) {
        let com_acento = "ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝŔÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿŕ ";
        let sem_acento = "AAAAAAACEEEEIIIIDNOOOOOOUUUUYRsBaaaaaaaceeeeiiiionoooooouuuuybyr ";

        novastr = "";
        for (i = 0; i < text.length; i++) {
            troca = false;
            for (a = 0; a < com_acento.length; a++) {
                if (text.substr(i, 1) == com_acento.substr(a, 1)) {
                    novastr += sem_acento.substr(a, 1);
                    troca = true;
                    break;
                }
            }
            if (troca == false) {
                novastr += text.substr(i, 1);
            }
        }

        novastr = novastr.replace(/ /g, "%20");

        return novastr;
    },

    url_image: function (text) {
        let com_acento = "ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝŔÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿŕ ";
        let sem_acento = "AAAAAAACEEEEIIIIDNOOOOOOUUUUYRsBaaaaaaaceeeeiiiionoooooouuuuybyr ";

        novastr = "";
        for (i = 0; i < text.length; i++) {
            troca = false;
            for (a = 0; a < com_acento.length; a++) {
                if (text.substr(i, 1) == com_acento.substr(a, 1)) {
                    novastr += sem_acento.substr(a, 1);
                    troca = true;
                    break;
                }
            }
            if (troca == false) {
                novastr += text.substr(i, 1);
            }
        }

        novastr = novastr.replace(/ /g, '_');
        novastr = novastr.replace(/[-/]/g, '_');

        novastr = novastr.toLowerCase();

        return novastr;
    },

    active: function () {
        //ESCOLHER UM
        $(document).on('click', '.compra-assistida .step.step_1 .content a', function (e) {
            e.preventDefault();

            $('.compra-assistida .step.step_1 a').removeClass('active');
            $(this).addClass('active');
        });

        //PRECO
        $(document).on('click', '.compra-assistida .step.step_5 .content a', function (e) {
            e.preventDefault();

            $('.compra-assistida .step.step_5 a').removeClass('active');
            $(this).addClass('active');
        });

        //ESCOLHER VARIOS
        $(document).on('click', '.compra-assistida .step:not(.step_1):not(.step_5) .content a', function (e) {
            e.preventDefault();
            $(this).toggleClass('active');
        });
    },

    pronto: function () {
        $('.compra-assistida main .footer #send').on('click', function (e) {
            e.preventDefault();

            let url = '';
            url += $('.compra-assistida .step_1 a.active').attr('href'); //CATEGORIA - 1
            url += '/';

            //APLICACAO - 1
            $('.compra-assistida .step_2 a.active').each(function (a, b) {
                url += $(b).data('name') + '/';
            });

            //COR - 2
            $('.compra-assistida .step_4 a.active').each(function (a, b) {
                url += $(b).data('name') + '/';
            });

            //CARACTERISTICAS - 3
            $('.compra-assistida .step_3 a.active').each(function (a, b) {
                url += $(b).data('name') + '/';
            });

            url += $('.compra-assistida .step_5 a.active').data('slug'); //PREÇO

            url += '?PS=100&map=c,c,';
            //MAP: APLICACAO - 1
            $('.compra-assistida .step_2 a.active').each(function (a, b) {
                url += $(b).data('map') + ',';
            });

            //MAP: COR - 2
            $('.compra-assistida .step_4 a.active').each(function (a, b) {
                url += $(b).data('map') + ',';
            });

            //MAP: CARACTERISTICAS - 3
            $('.compra-assistida .step_3 a.active').each(function (a, b) {
                url += $(b).data('map') + ',';
            });

            url += 'priceFrom'; //MAP - PREÇO

            console.log(url);

            window.open(url, '_blank');
        });
    },

    busca: function () {
        //BUSCAR ESCOLA
        $(".compra-assistida .lista.sub.categoria input").on('keyup', function () {
            if ($(this).val() === '') {
                $('.compra-assistida .lista.sub.categoria > ul li').show();
            } else {
                let string = $('.compra-assistida .lista.sub.categoria input[type="text"]').val();

                $('.compra-assistida .lista.sub.categoria > ul li').hide();
                $('.compra-assistida .lista.sub.categoria > ul li span').each(function (index, el) {
                    let thisText = $(el).text();
                    thisText = thisText.toLowerCase();
                    string = string.toLowerCase();

                    if (thisText.indexOf(string) != -1) {
                        $(el).parents('li').show();
                    }
                });
            }
        });
    },

    aplicacao: function (sub, name) {
        //EVITA REPETICAO DE APPEND
        $('.compra-assistida main .step.step_2 .content ul, .compra-assistida main .step.step_3 .content ul, .compra-assistida main .step.step_4 .content ul, .compra-assistida main .step.step_5 .content ul');

        $.ajax({
            url: 'https://dev.sandersdigital.com.br/wiler-k/compra_assistida.php',
            dataType: 'json',
            method: 'GET',
            action: 'GET',
            data: {
                parametro: 'https://www.wiler-k.com.br/api/catalog_system/pub/facets/search' + sub + '?map=c,c'
            },
            success: function (response) {
                console.log(response);

                //NOME DA CATEGORIA SELECIONADA
                let categoria = response.Departments[0].Name;

                //LISTA DE APLICAÇÕES
                let list = response.SpecificationFilters['Aplicação'];

                if (response.SpecificationFilters['Aplicação'] === undefined && response.SpecificationFilters['Característica'] === undefined && response.SpecificationFilters['Cores'] === undefined) {
                    //NÃO HÁ APLICAÇÕES, CARACTERISTICAS E CORES - PULE PARA PREÇOS
                    $('.compra-assistida .box>.footer, .compra-assistida .timeline').attr('data-step', '5');

                    $('.box .step_1').fadeOut(300, function () {
                        setTimeout(function () {
                            $('.box .step_5').fadeIn(300);
                        }, 300);
                    });
                } else if (response.SpecificationFilters['Aplicação'] === undefined && response.SpecificationFilters['Característica'] === undefined) {
                    //NÃO HÁ APLICAÇÕES E CARACTERISTICAS - PULE PARA CORES OU ESTAMPAS
                    $('.compra-assistida .box>.footer, .compra-assistida .timeline').attr('data-step', '4');

                    $('.box .step_1').fadeOut(300, function () {
                        setTimeout(function () {
                            $('.box .step_4').fadeIn(300);
                        }, 300);
                    });
                } else if (response.SpecificationFilters['Aplicação'] === undefined) {
                    //NÃO HÁ APLICAÇÕES - PULE PARA CARACTERISTICAS
                    $('.compra-assistida .box>.footer, .compra-assistida .timeline').attr('data-step', '3');

                    $('.box .step_1').fadeOut(300, function () {
                        setTimeout(function () {
                            $('.box .step_3').fadeIn(300);
                        }, 300);
                    });
                }

                //ONDE VOCÊ VAI APLICAR?
                $(list).each(function (a, b) {
                    let content = '';
                    content += '<li>';
                    content += '<a href="' + b.LinkEncoded + '" data-name="' + steps.retira_acentos(b.Name) + '" data-map="' + b.Map + '" data-quantity="' + b.Quantity + '">';
                    content += '<i><img src="/arquivos/imagem_aplicacao_' + steps.url_image(b.Name) + '.jpg" /></i>';
                    content += '<span>' + b.Name + '</span>';
                    content += '</a>';
                    content += '</li>';

                    $('.compra-assistida main .step.step_2 .content ul').append(content);
                });

                //LISTA DE CARACTERISTICAS
                let list2 = response.SpecificationFilters['Característica'];
                $(list2).each(function (a, b) {
                    let content = '';
                    content += '<li>';
                    content += '<a href="' + b.LinkEncoded + '" data-name="' + steps.retira_acentos(b.Name) + '" data-map="' + b.Map + '" data-quantity="' + b.Quantity + '">';
                    content += '<i><img src="/arquivos/ico_' + steps.url_image(b.Name) + '.png" /></i>'
                    content += '<span>' + b.Name + '</span>';
                    content += '</a>';
                    content += '</li>';

                    $('.compra-assistida main .step.step_3 .content ul').append(content);
                });

                //LISTA DE CORES OU ESTAMPAS
                let list3 = response.SpecificationFilters['Cores'];
                $(list3).each(function (a, b) {
                    console.log(list3);
                    let content = '';
                    content += '<li>';
                    content += '<a href="' + b.LinkEncoded + '" data-name="' + steps.retira_acentos(b.Name) + '" data-map="' + b.Map + '" data-quantity="' + b.Quantity + '">';
                    content += '<span>' + b.Name + '</span>';
                    content += '</a>';
                    content += '</li>';

                    $('.compra-assistida main .step.step_4 .content ul').append(content);
                });

                //LISTAGEM PREÇOS
                let list4 = response.PriceRanges;
                $(list4).each(function (a, b) {
                    let content = '';
                    content += '<li>';
                    content += '<a href="' + b.LinkEncoded + '" data-name="' + steps.retira_acentos(b.Name) + '" data-slug="' + b.Slug + '" data-quantity="' + b.Quantity + '">';
                    content += '<span>' + b.Name + '</span>';
                    content += '</a>';
                    content += '</li>';

                    $('.compra-assistida main .step.step_5 .content ul').append(content);
                });
            }
        });
    },

    step_1: function () {
        $.ajax({
            url: 'https://dev.sandersdigital.com.br/wiler-k/compra_assistida.php',
            dataType: 'json',
            method: 'GET',
            action: 'GET',
            data: {
                parametro: 'https://www.wiler-k.com.br/api/catalog_system/pub/category/tree/3'
            },
            success: function (response) {
                //STEP 1 - CATEGORIAS
                $(response).each(function (a, b) {
                    let content1 = '';
                    content1 += '<li data-name="' + b.name + '">';
                    content1 += '<a href="' + b.url + '" data-id="' + b.id + '" data-children="' + b.hasChildren + '">';
                    content1 += '<i><img src="/arquivos/imagem_' + steps.url_image(b.name) + '.jpg" alt="' + b.name + '" /></i>';
                    content1 += '<span>' + b.name + '</span>';
                    content1 += '</a>';
                    content1 += '</li>';

                    $('.box .step_1 .lista.categoria:not(.sub) > ul').append(content1);
                });
                //FIM - STEP 1 - CATEGORIAS

                //STEP 1 - ESCOLHA A CATEGORIA
                $('.box .step_1 .lista.categoria li a').on('click', function (e) {
                    e.preventDefault();

                    //CATEGORIA SELECIONADA
                    let name = $(this).parents('li').data('name');

                    $.ajax({
                        url: 'https://dev.sandersdigital.com.br/wiler-k/compra_assistida.php',
                        dataType: 'json',
                        method: 'GET',
                        action: 'GET',
                        data: {
                            parametro: 'https://www.wiler-k.com.br/api/catalog_system/pub/category/tree/3'
                        },
                        success: function (response) {
                            $('.box .step_1 .lista.categoria').fadeOut(300, function () {
                                setTimeout(function () {
                                    $('.box .step_1 .lista.sub.categoria').removeClass('dis-none');
                                }, 300);
                            });

                            //TRAZ A CATEGORIA SELECIONADA
                            $(response).each(function (a, b) {
                                if (b.name === name) {
                                    $.each(b.children, function (c, child) {

                                        let sub = child.url.split('www.wiler-k.com.br')[1];

                                        console.log(b);

                                        let content2 = '';
                                        content2 += '<li data-name="' + b.name + '" data-sub="' + sub + '" class="">';
                                        content2 += '<a href="' + child.url + '" data-sub="' + sub + '" data-id="' + child.id + '" data-children="' + child.hasChildren + '">';
                                        content2 += '<i><img src="/arquivos/imagem_' + steps.url_image(b.name) + '_' + steps.url_image(child.name) + '.jpg" alt="' + child.name + '" /></i>';
                                        content2 += '<span>' + child.name + '</span>';
                                        content2 += '</a>';
                                        content2 += '</li>';

                                        $('.box .step_1 .lista.sub.categoria > ul').append(content2);
                                    });
                                }
                            });
                        }
                    });
                });

                $(document).on('click', '.box .step_1 .lista.sub.categoria ul li a', function (e) {
                    e.preventDefault();

                    steps.aplicacao($(this).data('sub'), $(this).text());
                });
            }
        });
    }
};

var footer = {
    pagination: function () {
        $('.compra-assistida .box>.footer li a:not(#send)').on('click', function (e) {
            e.preventDefault();

            var change_step = $('.compra-assistida .box>.footer, .compra-assistida .timeline');

            if ($(this).hasClass('next')) {

                let i = parseInt(change_step.attr('data-step')) + 1;

                if (i < 6) {
                    //NÃO ULTRAPASSA 5 PÁGINAS
                    change_step.attr('data-step', i);
                    $('.box .step').fadeOut(300, function () {
                        setTimeout(function () {
                            $('.box .step_' + i).fadeIn(300);
                        }, 300);
                    });
                }

                //EXISTE OPÇÕES DISPONIVEIS?
                //EXISTE UMA OPÇÃO DISPONIVEL SELECIONADA?
                // if ($('.step.step_' + parseInt(change_step.attr('data-step'))).find('a.active').length) {
                // } else {
                // 	swal("Oops!", "Escolha uma opção!", "warning");
                // }

                // if ($('.step.step_' + parseInt(change_step.attr('data-step'))).find('a').length) {
                // }

            } else if ($(this).hasClass('prev')) {
                let i = parseInt(change_step.attr('data-step')) - 1;
                change_step.attr('data-step', i);
                $('.box .step').fadeOut(300, function () {
                    setTimeout(function () {
                        $('.box .step_' + i).fadeIn(300);
                    }, 300);
                });
            }
        });
    }
};

//ATIVA OPÇÕES
steps.active();

//BUSCA AS SUB-CATEGORIAS
steps.busca();

//CONSTRÓI URL FINAL
steps.pronto();

//STEP 1
steps.step_1();

//FOOTER
footer.pagination();
})();

var geral = function () {
    var t = function () {
            $("#newsletterButtonOK").val("CADASTRAR")
        },
        status = function () {
            $.ajax({
                type: 'GET',
                url: '/api/vtexid/pub/authenticated/user',
            }).done(function (user) {
                if (user != null) {
                    $('header .row-1 .icones>ul li.btn_account li a[href="/account"]').addClass('dis-none');
                    $('header .row-1 .icones>ul li.btn_account a[href="/no-cache/user/logout"]').removeClass('dis-none');
                    $('header .row-1 .icones>ul li.btn_account a[href="/account/orders#/profile"]').removeClass('dis-none');
                }
            });
        },
        e = function () {
            $("header .icones .btn_account > a").on("click", function (t) {
                t.preventDefault(), $(this).next(".hover").toggleClass("active")
            })
        },
        a = function () {
            $("header .hamburger").on("click", function () {
                $("header .row-2, body, #overlay").toggleClass("active")
            }), $("header .close_menu").on("click", function () {
                $("header .hamburger").trigger("click")
            })
        },
        i = function () {
            $("header .row-2 .menu nav>ul>li p a").on("click", function (t) {
                t.preventDefault(), $("header .row-2 .menu .head").text($(this).attr("title")), $("header .row-2 .menu .back, header .row-2 .menu nav>ul>li p").addClass("active"), $(this).parents("li").find("div.submenu").addClass("active")
            }), $("header .row-2 .menu .back").on("click", function () {
                $("header .row-2 .menu .head").text("Departamentos"), $("header .row-2 .menu nav>ul>li .submenu, header .row-2 .menu .back, header .row-2 .menu nav>ul>li p").removeClass("active")
            })
        },
        o = function () {
            $(".toggle .title").on("click", function () {
                $(this).toggleClass("active"), $(this).next("ul").slideToggle()
            })
        };
    (function () {
        $("li.helperComplement").remove()
    })(), t(), status(), $("body").width() < $mobile && (e(), a(), i(), o())
}(),
home = function () {
    var t = function () {
            $(".home .full_banner ul").slick({
                infinite: !1,
                autoplay: !0,
                autoplaySpeed: 4e3,
                arrows: !0,
                dots: !0,
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }, {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }]
            }), $(".home .full_banner .desktop").removeClass("loading")
        },
        e = function () {
            $(".selecionado_voce .prateleira ul").each(function (t, e) {
                $("body").width() > 1024 ? $(this).find("li").length > 4 && $(this).slick({
                    autoplay: !1,
                    autoplaySpeed: 4e3,
                    infinite: !1,
                    arrows: !0,
                    dots: !1,
                    slidesToShow: 4,
                    slidesToScroll: 1
                }) : $(this).slick({
                    autoplay: !1,
                    autoplaySpeed: 4e3,
                    infinite: !1,
                    arrows: !0,
                    dots: !1,
                    slidesToShow: 2,
                    slidesToScroll: 1
                })
            }), $(".tabs li a").on("click", function (t) {
                t.preventDefault(), $(".tabs li a").removeClass("active"), $(this).addClass("active"), $(".option").removeClass("active"), $(".option.prateleira-" + $(this).data("id")).addClass("active")
            })
        },
        a = function () {
            $.ajax({
                type: "GET",
                url: "https://blog.wiler-k.com.br/wp-json/wp/v2/posts/?_embed",
                success: function (t) {
                    for (i = 0; i < t.length; i++) {
                        var e = t[i],
                            a = "";
                        a += '<li data-id="' + e.id + '">', a += "<h3>" + e.title.rendered + "</h3>", a += '<div class="description"><p>' + e.excerpt.rendered + "</p></div>", a += '<a href="' + e.link + '" target="_blank">Leia mais ></a>', a += "</li>", $(".nosso_blog ul").append(a)
                    }
                    $(".nosso_blog ul").slick({
                        autoplay: !0,
                        autoplaySpeed: 4e3,
                        infinite: !1,
                        arrows: !0,
                        dots: !1,
                        slidesToShow: 4,
                        slidesToScroll: 1,
                        responsive: [{
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 1
                            }
                        }, {
                            breakpoint: 600,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1
                            }
                        }]
                    })
                }
            })
        },
        o = function () {
            $(".destaques ul li").length > 4 && $(".destaques ul").slick({
                autoplay: !0,
                autoplaySpeed: 4e3,
                infinite: !1,
                arrows: !0,
                dots: !1,
                slidesToShow: 4,
                slidesToScroll: 1,
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                }, {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }]
            })
        },
        n = function () {
            $(".venda_assistida ul").slick({
                autoplay: !0,
                autoplaySpeed: 4e3,
                infinite: !1,
                arrows: !1,
                dots: !1,
                slidesToShow: 2,
                slidesToScroll: 1,
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                }, {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }]
            })
        },
        l = function () {
            $(".para_voce ul, .tipbar ul").slick({
                autoplay: !0,
                autoplaySpeed: 4e3,
                infinite: !0,
                arrows: !1,
                dots: !1,
                centerMode: !0,
                variableWidth: !0,
                slidesToShow: 2,
                slidesToScroll: 1,
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                }, {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }]
            })
        };
    $("body").hasClass("home") && (t(), e(), a(), o(), $("body").width() < $mobile && (n(), l()))
}(),
produto = function () {
    var t = {
        galeria: function () {
            $.ajax({
                type: 'GET',
                url: '/api/catalog_system/pub/products/search?fq=productId:' + skuJson.productId
            }).
            done(function (res) {
                let images = res[0].items[0].images;

                console.log(images);

                $.each(images, function (a, b) {
                    var img = new Image();

                    img.onload = function () {
                        if (this.width === this.height) {
                            console.log('mil por mil');
                            $('#thumbs ul').append('<li><img src="' + b.imageUrl + '" /></li>');
                        } else {
                            $('#galeria, .produto .btn.galeria').removeClass('dis-none');
                            $('#galeria ul').append('<li><img src="' + b.imageUrl + '" /></li>');
                        }
                    }

                    img.src = b.imageUrl;
                });
            });
        },
        sku_selection: function () {
            if (skuJson.skus.length <= 1) $(".sku_list").show();
            else {
                $(".sku_selection").show();
                let t = skuJson.skus;
                $(".skuList input").each(function () {
                    for (var e = $(this).data("value"), a = 0; a < t.length; a++) {
                        let i = t[a];
                        i.dimensions.Cores === e && $(this).next("label").prepend('<img src="' + i.image + '" alt="' + i.skuname + '" />')
                    }
                })
            }
        },
        thumbs: function () {
            $(window).load(function () {
                console.log('thumbs');

                //THUMBS
                $(".produto #thumbs ul").slick({
                    autoplay: !1,
                    autoplaySpeed: 4e3,
                    infinite: !0,
                    arrows: !0,
                    dots: !0,
                    variableWidth: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    responsive: [{
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            variableWidth: false,
                            infinite: false
                        }
                    }, {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            variableWidth: false,
                            infinite: false
                        }
                    }]
                });

                ////THUMBS
                for (let e = 0; e < $(".produto #thumbs .slick-dots li").length; e++) {
                    var t = $(".produto #thumbs .slick-dots li")[e];
                    $(t).attr("data-index", e)
                }

                for (let a = 0; a < $('.produto #thumbs .slick-list li:not(".slick-cloned")').length; a++) {
                    t = $('.produto #thumbs .slick-list li:not(".slick-cloned")')[a];
                    var e = $(t).find("img").attr("src");
                    $(".produto #thumbs .slick-dots li[data-index=" + a + "]").css("background-image", "url(" + e + ")")
                }

                //ZOOM APENAS DESKTOP
                if ($("body").width() > $mobile) {
                    $("#thumbs .slick-slide").zoom();
                }

                //GALERIA 
                $(".produto #galeria ul").slick({
                    autoplay: !1,
                    autoplaySpeed: 4e3,
                    arrows: !0,
                    dots: !0,
                    slidesToShow: 1,
                    slidesToScroll: 1
                });

                //GALERIA
                for (let e = 0; e < $(".produto #galeria .slick-dots li").length; e++) {
                    var t = $(".produto #galeria .slick-dots li")[e];
                    $(t).attr("data-index", e)
                }

                for (let a = 0; a < $('.produto #galeria .slick-list li:not(".slick-cloned")').length; a++) {
                    t = $('.produto #galeria .slick-list li:not(".slick-cloned")')[a];
                    var e = $(t).find("img").attr("src");
                    $(".produto #galeria .slick-dots li[data-index=" + a + "]").css("background-image", "url(" + e + ")")
                }
            });
        },
        typeofproduct: function () {
            var t = vtxctx.departmentName;
            $(".productType");
            $('<span class="typeofprod"></span>').insertAfter(".skuBestPrice"), "TECIDOS PARA SOFÁ" == t || "TECIDOS PARA CORTINAS" == t || "CORINO E COURVIN" == t ? ($(".produto .typeofprod:not(.small)").text("/Metro Linear"), $(".typeofprod.small").text("m")) : "PAPEL DE PAREDE" == t || ($(".produto .typeofprod:not(.small)").text("/Un."), $(".typeofprod.small").text("un."))
        },
        range: function () {
            $(".product_total span").text(skuJson.skus[0].bestPriceFormated);
            var t = skuJson.skus[0].bestPrice;

            function e(t, e) {
                t.textContent = e
            }
            $('input[type="range"]').rangeslider({
                polyfill: !1,
                onInit: function () {
                    e($(".rangeslider__handle", this.$range)[0], this.value)
                }
            }).on("input", function (a) {
                e($(".rangeslider__handle", a.target.nextSibling)[0], this.value), $(".produto .box_count .input input").val(this.value);
                var i = t * this.value;
                i = (i = (i /= 100).toFixed(2)).replace(".", ","), $(".product_total span").text("R$ " + i)
            })
        },
        change_price: function () {
            if (typeof selectedToBuy === 'undefined' || typeof selectedToBuy[0] === 'undefined') {
                int = skuJson.skus[0].sku;
            } else {
                int = selectedToBuy[0]
            }
            var t = skuJson.skus.filter(function (t) {
                    return t.sku == int
                }),
                e = $(".box_count .input input").val(),
                a = t[0].bestPrice * e;
            a = (a = (a /= 100).toFixed(2)).replace(".", ","), $(".product_total span").text("R$ " + a)
        },
        update_price: function () {
            $(window).load(function () {
                $(".product_sku_list .topic.Cores li.skuList span input").first().trigger("click"), t.change_price()
            }), $(".topic.Cores li.skuList span input").on("change", function () {
                t.change_price(), $(".produto .thumbs").removeClass("slick-initialized slick-slider slick-dotted"), t.thumbs()
            })
        },
        qtdMax: function () {
            //LIMITADOR PARA O CONTADOR
            //SKU ÚNICO
            if (selectedToBuy.length === 0) {
                $('.box_count .input input').attr('data-max', skuJson.skus[0].availablequantity);
            } else {
                //MULTIPLOS SKUS
                let sku = parseInt(selectedToBuy[0]);
                $.each(skuJson.skus, function (index, item){
                    if (item.sku === sku) {
                        $('.box_count .input input').attr('data-max', item.availablequantity);            
                    }
                }); 
            }
        },
        count: function () {
            $(".count").on("click", function (e) {
                t.qtdMax();
                var input = $(".box_count .input input");

                if (e.preventDefault(), $(this).hasClass("count-mais")) {
                    var a = parseInt(input.attr("value"));
                    if (a < parseInt(input.attr('data-max'))) {
                        a++;
                    }
                } else if ($(this).hasClass("count-menos")) {
                    (a = parseInt(input.attr("value"))) > 1 && a--;
                }
                input.attr("value", a), t.change_price();
                $('.buy-in-page-quantity').trigger('quantityChanged.vtex', [skuJson.productId, a]);
            })
        },
        calcula_frete: function () {
            $('.product_frete form input[type="text"]').mask("00000-000"), $(".product_frete form").on("submit", function (t) {
                if (t.preventDefault(), 9 != $('.product_frete form input[type="text"]').val().length) $('.product_frete form input[type="text"]').addClass("active");
                else {
                    $(".product_frete form input").removeClass("active");
                    var e = skuJson.skus[0].sku,
                        a = $('.product_frete form input[type="text"]').val().replace("-", ""),
                        i = $(".product_qtd .column_1 .box_count .input input[type=number]").val();
                    $.ajax({
                        url: "/frete/calcula/" + e + "?shippinCep=" + a + "&quantity=" + i,
                        type: "GET",
                        dataType: "HTML",
                        success: function (t) {
                            $(".product_frete .table").html(""), $(".product_frete .table").append('<a href="" class="remove_frete">X</a>' + t)
                        }
                    })
                }
            }), $(document).on("click", ".remove_frete", function (t) {
                t.preventDefault(), $('.product_frete form input[type="text"]').val(""), $(".product_frete .table").html("")
            })
        },
        slick_medidas: function () {
            $(".slider-for").length && ($(".slider-for").slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: !1,
                fade: !0,
                asNavFor: ".slider-nav"
            }), $(".slider-nav").slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                asNavFor: ".slider-for",
                dots: !0,
                centerMode: !0,
                focusOnSelect: !0
            }))
        },
        changeImgSku: function () {
            $('.skuList .group_0 label').on('click', function (event) {
                setTimeout(function () {
                    t.qtdMax();
                    $('.produto main .product_info>.column_2 .product_qtd .column_1 .box_count .input input[type=number]').val(1);
                    t.change_price();
                }, 500);

                let name = $(this).children('img').attr('alt');
                $(skuJson.skus).each(function (index, el) {
                    let nameSku = el.skuname
                    if (name === nameSku) {
                        $('#slick-slide00 img').attr('src', el.image);
                    }
                });
            });
        },
        prateleira: function () {
            function t() {
                $(".produto .prateleira ul").slick({
                    autoplay: !1,
                    autoplaySpeed: 4e3,
                    infinite: !0,
                    arrows: !0,
                    dots: !1,
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    responsive: [{
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1
                        }
                    }, {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    }]
                })
            }
            $("body").width() <= 768 ? t() : $(".produto .prateleira ul li").length > 4 && t()
        }
    };
    if ($('body').hasClass('produto')) {
        t.galeria();
        t.thumbs();
        t.sku_selection();
        t.typeofproduct();
        t.update_price();
        $(window).load(function(){t.qtdMax();});
        t.count();
        t.slick_medidas();
        t.changeImgSku();
        t.prateleira();
    };
}(),
departamento = function () {
    var t = function () {
            $(window).load(function () {
                $(document).find(".filtro_tipos-de-tecidos h5").after('<article class="busca_filtro"><input type="text" id="buscaFiltro" placeholder="Busque por palavra-chave" /></article>')
            }), $(document).on("keyup", "#buscaFiltro", function () {
                var t = "";
                t = $(this).val();
                $(".search-multiple-navigator .filtro_tipos-de-tecidos label").css("display", "none"), $(".search-multiple-navigator .filtro_tipos-de-tecidos label").each(function (e, a) {
                    var i = $(a).text();
                    i = i.toLowerCase(), t = t.toLowerCase(), -1 != i.indexOf(t) && $(a).css("display", "flex")
                })
            })
        },
        e = function () {
            var t = [];
            for (let i = 0; i < $(".prateleira ul li article").length; i++) {
                const o = $(".prateleira ul li article")[i];
                var e = $(o).attr("data-category"),
                    a = $(o).attr("data-link");
                t.push({
                    name: e,
                    link: a
                })
            }
            var i, o, n = (o = "name", (i = t).map(t => t[o]).map((t, e, a) => a.indexOf(t) === e && e).filter(t => i[t]).map(t => i[t]));
            for (let t = 0; t < n.length; t++) {
                const e = n[t];
                console.log("Nome: " + e.name + " | Link: " + e.link), $(".sugestions ul").append('<li><a href="' + e.link + '" title="' + e.name + '">' + e.name + "</a></li>")
            }
        },
        a = function () {
            $(window).load(function () {})
        },
        i = function () {
            $(".ordenar_por .content ul li label").on("click", function (t) {
                t.preventDefault();
                var e = $(".departamento main .ordenar_por .content ul li label.active").data("name");
                $(".ordenar_por .content ul li label").removeClass("active"), $('.flags ul li[data-name="' + e + '"]').trigger("click"), setTimeout(() => {
                    $(this).addClass("active"), $('.search-multiple-navigator fieldset div label input[rel="' + $(".ordenar_por .content ul li label.active input").data("value") + '"]').trigger("click")
                }, 2e3)
            })
        },
        o = function () {
            $(document).on("change", ".search-multiple-navigator label input", function () {
                var t = $(this).parent().text(),
                    e = $(this).parent().attr("title"),
                    a = '<li data-name="' + e + '"><p>' + t + "</p><span>x</span></li>";
                $(this).parent().hasClass("sr_selected") ? $(".flags ul").append(a) : $('.flags .content ul li[data-name="' + e + '"]').remove()
            }), $(document).on("click", ".flags li", function (t) {
                t.preventDefault(), $('.search-multiple-navigator label[title="' + $(this).data("name") + '"]').trigger("click"), $('.departamento .ordenar_por .content ul li label[data-name="' + $(this).data("name") + '"], .ordenar_por .content ul li label').removeClass("active")
            }), $(".flags .clear a").on("click", function (t) {
                t.preventDefault(), $(".flags .content ul li").trigger("click")
            })
        },
        n = function () {
            $(".search-multiple-navigator input[type='checkbox']").vtexSmartResearch()
        },
        l = function () {
            $('.filter_buttons a[title="Ordenar"]').on("click", function (t) {
                t.preventDefault(), $(".ordenar_por").toggleClass("active")
            }), $(".ordenar_por .title, .ordenar_por .btn_aplicar").on("click", function (t) {
                t.preventDefault(), $('.filter_buttons a[title="Ordenar"]').trigger("click")
            })
        },
        r = function () {
            $('.filter_buttons a[title="Filtrar"]').on("click", function (t) {
                t.preventDefault(), $(".search_navigator").toggleClass("active")
            }), $(".search_navigator .head, .search_navigator .btn_aplicar").on("click", function (t) {
                t.preventDefault(), $('.filter_buttons a[title="Filtrar"]').trigger("click")
            })
        };
    $("body").hasClass("departamento") && (t(), e(), a(), o(), i(), n(), $("body").width() < $mobile && (l(), r()))
}(),
erros = void($(".box_search input").keypress(function (t) {
    if ("13" == (t.keyCode ? t.keyCode : t.which)) {
        var e = $(this).val();
        location.href = "/" + e
    }
}), void $(".box_search button").on("click", function () {
    location.href = "/" + $(".box_search input").val()
})),
carrinho = function () {
    var t = {
            no_estoque: function () {
                $(".no-estoque").on("click", function () {
                    $(this).removeClass("active")
                })
            },
            openCart: function () {
                $("body").width() < $mobile && $("#cart-lateral .header, #cart-lateral .cart-close").on("click", function () {
                    $("#cart-lateral, #overlay, body").toggleClass("active")
                })
            },
            qtyCart: function () {
                var t = 0;
                vtexjs.checkout.getOrderForm().done(function (e) {
                    for (var a = e.items.length - 1; a >= 0; a--) t = parseInt(t) + parseInt(e.items[a].quantity);
                    $("header li.btn_checkout span").text(t)
                })
            },
            toggleCart: function () {
                $("header .row-1 .icones ul li.btn_checkout a").on("click", function (t) {
                    t.preventDefault(), $("#overlay, #cart-lateral, body").toggleClass("active")
                }), $("#cart-lateral .cart-close").on("click", function () {
                    $("header .row-1 .icones ul li.btn_checkout a").trigger("click"), $("#cart-message li").remove()
                })
            },
            calculateShipping: function () {
                "" != $('#search-cep input[type="text"]').val() && vtexjs.checkout.getOrderForm().then(function (t) {
                    if (null === localStorage.getItem("cep")) var e = $('#search-cep input[type="text"]').val();
                    else e = localStorage.getItem("cep");
                    var a = {
                        postalCode: e,
                        country: "BRA"
                    };
                    return $("#cart-lateral .footer ul li span.frete .box-1").fadeOut(300, function () {
                        $("#cart-lateral .footer ul li span.frete .box-2").fadeIn(300)
                    }), vtexjs.checkout.calculateShipping(a)
                }).done(function (t) {
                    if (0 == t.totalizers.length) $("#cart-lateral .value-frete").text("Você não tem itens no carrinho!");
                    else {
                        var e = t.totalizers[1].value / 100;
                        e = e.toFixed(2).replace(".", ",").toString(), $("#cart-lateral .value-frete").text("R$: " + e);
                        var a = $('#search-cep input[type="text"]').val();
                        localStorage.setItem("cep", a), $('#search-cep input[type="text"]').val(a)
                    }
                })
            },
            automaticCalculateShipping: function () {
                null != localStorage.getItem("cep") && ($('#search-cep input[type="text"]').val(localStorage.getItem("cep")), t.calculateShipping())
            },
            fadeAction: function () {
                $("#cart-lateral .footer ul li span.frete a").on("click", function (t) {
                    t.preventDefault(), $("#cart-lateral .footer ul li span.frete a").fadeOut(300, function () {
                        $("#cart-lateral .footer ul li span.frete .box-1 div").fadeIn(300)
                    })
                }), $("#cart-lateral .return-frete").on("click", function () {
                    $("#cart-lateral .footer ul li span.frete .box-2").fadeOut(300, function () {
                        $("#cart-lateral .footer ul li span.frete .box-1").fadeIn(300)
                    })
                })
            },
            calculaFrete: function () {
                $('#search-cep input[type="text"]').mask("00000-000"), $("#search-cep input[type=submit]").on("click", function (e) {
                    e.preventDefault(), 9 === $('#search-cep input[type="text"]').val().length ? (t.calculateShipping(), $('#search-cep input[type="text"]').removeClass("active")) : $('#search-cep input[type="text"]').addClass("active")
                }), $("#search-cep input[type=text]").on("keypress", function (e) {
                    if ("13" == a)
                        if (9 === $('#search-cep input[type="text"]').val().length) {
                            var a = e.keyCode ? e.keyCode : e.which;
                            t.calculateShipping(), $('#search-cep input[type="text"]').removeClass("active")
                        } else $('#search-cep input[type="text"]').addClass("active")
                })
            },
            cartLareal: function () {
                $("#cart-lateral .content, .add_cart, .prateleira article").removeClass("loading"), t.qtyCart(), vtexjs.checkout.getOrderForm().done(function (t) {
                    if (0 != t.value ? (total_price = t.value / 100, total_price = total_price.toFixed(2).replace(".", ",").toString(), $("#cart-lateral .footer .total-price").text("R$: " + total_price)) : $("#cart-lateral .footer .total-price, #cart-lateral .value-frete").text("R$: 0,00"), 0 != t.totalizers.length ? (sub_price = t.totalizers[0].value / 100, sub_price = sub_price.toFixed(2).replace(".", ",").toString(), $("#cart-lateral .footer .value-sub-total").text("R$: " + sub_price)) : $("#cart-lateral .footer .value-sub-total").text("R$: 0,00"), 0 != t.items) {
                        for (var e = 0, a = t.items.length - 1; a >= 0; a--) e = parseInt(e) + parseInt(t.items[a].quantity);
                        $("#cart-lateral .header .total-items").text(e)
                    } else $("#cart-lateral .header .total-items").text("0");
                    for ($("#cart-lateral .content ul li").remove(), a = 0; a < t.items.length; a++) {
                        price_item = t.items[a].price / 100, price_item = price_item.toFixed(2).replace(".", ",").toString();
                        var i = "";
                        i += '<li data-index="' + a + '" data-id="' + t.items[a].productId + '">', i += '<div class="image"><img src="' + t.items[a].imageUrl + '" alt="' + t.items[a].name + '"/></div>', i += '<div class="text">', i += "<p>" + t.items[a].name + "</p>", i += '<ul class="ft">', i += '<li data-index="' + a + '">', i += '<div class="box-count">', i += '<a href="" class="count count-down">-</a>', i += '<input type="number" value="' + t.items[a].quantity + '" />', i += '<a href="" class="count count-up">+</a>', i += "</div>", i += "</li>", i += "<li>", i += '<p class="price">R$: ' + price_item + "</p>", i += "</li>", i += "</ul>", i += "</div>", i += '<span class="removeUni"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 129 129" enable-background="new 0 0 129 129" style=" width: 16px; height: 16px; fill: #7d7d7d;"> <g> <g> <path d="m10.5,31.2h4.5l10.5,87.7c0.2,2.1 2,3.6 4.1,3.6h69.8c2.1,0 3.8-1.5 4.1-3.6l10.5-87.7h4.5c2.3,0 4.1-1.8 4.1-4.1s-1.8-4.1-4.1-4.1h-8.2-23.2v-12.5c0-2.3-1.8-4.1-4.1-4.1h-37c-2.3,0-4.1,1.8-4.1,4.1v12.5h-23.2-8.2c-2.3,0-4.1,1.8-4.1,4.1s1.9,4.1 4.1,4.1zm39.6-16.6h28.8v8.4h-28.8v-8.4zm55.6,16.6l-9.9,83.2h-62.6l-9.9-83.2h82.4z"></path> <path d="m50.9,101.9c2.3,0 4.1-1.8 4.1-4.1v-49.1c0-2.3-1.8-4.1-4.1-4.1-2.3,0-4.1,1.8-4.1,4.1v49.1c0,2.3 1.8,4.1 4.1,4.1z"></path> <path d="m78.1,101.9c2.3,0 4.1-1.8 4.1-4.1v-49.1c0-2.3-1.8-4.1-4.1-4.1s-4.1,1.8-4.1,4.1v49.1c0,2.3 1.9,4.1 4.1,4.1z"></path> </g> </g></svg></span>', i += "</li>", $("#cart-lateral .content > ul").prepend(i)
                    }
                })
            },
            changeQuantity: function () {
                $(document).on("click", "#cart-lateral .count", function (e) {
                    e.preventDefault(), $("#cart-lateral .content").addClass("loading");
                    var a = $(this).siblings('input[type="number"]').val();
                    $(this).hasClass("count-up") ? (a++, $(this).siblings('input[type="number"]').removeClass("active"), $(this).siblings('input[type="number"]').val(a)) : $(this).hasClass("count-down") && (1 != $(this).siblings('input[type="number"]').val() ? (a--, $(this).siblings('input[type="number"]').val(a), $("#cart-lateral .no-estoque").removeClass("active")) : $(this).siblings('input[type="number"]').addClass("active"));
                    var i = $(this).parents("li").data("index");
                    data_quantity = $(this).parents("li").find('.box-count input[type="number"]').val(), vtexjs.checkout.getOrderForm().then(function (e) {
                        parseInt(e.items.length);
                        var a = i;
                        item = e.items[a], console.log(item.id), vtexjs.catalog.getProductWithVariations(item.productId).done(function (e) {
                            console.log(e);
                            var a = parseInt(data_quantity),
                                o = e.skus.filter(function (t) {
                                    return t.sku == item.id
                                });
                            console.log(o), a > o[0].availablequantity && ($(this).parents("li").find(".box-count").addClass("active"), $("#cart-lateral .no-estoque").fadeIn(), setTimeout(() => {
                                $("#cart-lateral .no-estoque").fadeOut()
                            }, 3e3), t.cartLareal()), vtexjs.checkout.getOrderForm().then(function (t) {
                                var e = i;
                                item = t.items[e];
                                var a = {
                                    index: i,
                                    quantity: data_quantity
                                };
                                return vtexjs.checkout.updateItems([a], null, !1)
                            }).done(function (e) {
                                t.cartLareal()
                            })
                        })
                    })
                })
            },
            removeItems: function () {
                $(document).on("click", "#cart-lateral .removeUni", function () {
                    $("#cart-lateral .content").addClass("loading");
                    var e = $(this).parents("li").data("index"),
                        a = $(this).siblings("li").find('.box-count input[type="number"]').val();
                    vtexjs.checkout.getOrderForm().then(function (t) {
                        var i = e,
                            o = (t.items[i], [{
                                index: e,
                                quantity: a
                            }]);
                        return vtexjs.checkout.removeItems(o)
                    }).done(function (e) {
                        t.cartLareal()
                    })
                })
            },
            removeAllItems: function () {
                $("#cart-lateral .clear").on("click", function () {
                    $("#cart-lateral .content").addClass("loading"), vtexjs.checkout.removeAllItems().done(function (e) {
                        t.cartLareal()
                    })
                })
            }
        },
        e = {
            aviso_cart: function (t) {
                $("header .row-1 .icones ul li.btn_checkout a").trigger("click"), $("#cart-message").append("<li><p>" + t + '</p><span>x</span><a href="/checkout">Abrir carrinho</a></li>'), $("#cart-message li").fadeIn(300, function () {
                    $("#cart-message li span").on("click", function () {
                        $("#cart-message li").fadeOut(300, function () {
                            $("#cart-message li").remove(), $("header .row-1 .icones ul li.btn_checkout a").trigger("click")
                        })
                    })
                }), setTimeout(() => {
                    $("#cart-message li").fadeOut(300, function () {
                        $("#cart-message li").remove()
                    })
                }, 4e3)
            },
            btn_buy: function () {
                window.alert = function () {
                    t.cartLareal();

                    if ($('body').width() > 768) {
                        e.aviso_cart("Produto adicionado com sucesso!");
                    } else {
                        //MOBILE
                        $("#cart-lateral").addClass("shake");
                        setTimeout(function () {
                            $("#cart-lateral").removeClass("shake");
                        }, 300);
                    }
                }
            }
        },
        a = function () {
            $(document).on("click", ".prateleira .add_cart", function () {
                $(this).addClass("loading");
                let e = $(this).parents("article").data("id"),
                    a = $(this).parents("article").find('.box input[type="number"]').val();
                vtexjs.catalog.getProductWithVariations(e).done(function (i) {
                    var o = {
                        id: e,
                        quantity: a,
                        seller: "1"
                    };
                    vtexjs.checkout.getOrderForm(), vtexjs.checkout.addToCart([o], null, 1).done(function (a) {
                        t.cartLareal(), $('.prateleira ul li article[data-id="' + e + '"]').addClass("add_more")
                    })
                })
            })
        },
        i = function () {
            $(document).on("click", ".prateleira .count", function (t) {
                t.preventDefault(), $("#cart-lateral .content").addClass("loading"), $(this).parents("article").addClass("loading");
                var e = $(this).parents("article").data("id"),
                    a = $(this).siblings('input[type="number"]').val();
                $(this).hasClass("count-mais") ? (a++, $(this).siblings('input[type="number"]').removeClass("active"), $(this).siblings('input[type="number"]').val(a), $('#cart-lateral .content ul li[data-id="' + e + '"]').find(".count-up").trigger("click")) : $(this).hasClass("count-menos") && (1 != $(this).siblings('input[type="number"]').val() ? (a--, $(this).siblings('input[type="number"]').val(a), $('#cart-lateral .content ul li[data-id="' + e + '"]').find(".count-down").trigger("click"), $("#cart-lateral .no-estoque").removeClass("active")) : $(this).siblings('input[type="number"]').addClass("active"))
            })
        };
    e.btn_buy(), a(), i(), t.no_estoque(), t.openCart(), t.toggleCart(), t.cartLareal(), t.changeQuantity(), t.removeItems(), t.removeAllItems()
}();