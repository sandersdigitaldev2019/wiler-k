if ($("body").width() < 1025) {
    $('header .row-2 .menu nav ul li > p > a > span').click(function (e) { 
        var goto = $(this).parent().attr('href');
        window.location.href = 'https://www.wiler-k.com.br' + goto;
        e.preventDefault();
    });			
}

if($("fieldset label input[type='checkbox']").prop("checked") == true){
    $("fieldset label input[type='checkbox']").prop("checked", false);
    location.reload();
}

removeFilter = function () {
    var count = 0;
    $('fieldset label input[type="checkbox"]').each( function () { 
        check = $(this).prop("checked") == true;
        if(check == true){
          	++count
            $(this).prop("checked", false);
        }
    });
    if(count > 0){
      location.reload();
    }
}

$("body").hasClass("departamento") && removeFilter();
