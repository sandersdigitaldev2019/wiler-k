<?php 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');  
header("Content-type: text/html; charset=utf-8"); 
?>

<?php
$url = 'http://blog.wiler-k.com.br/feed/';
$xml = simplexml_load_file($url);
 
echo $url;
if($xml !== false) {
  echo '<ul>';
  $index = 0;

	  foreach ($xml->channel->item as $key => $node) {

	    if($index < 4) {
		    $index++;

		  	$postDate = $originalDate = $node->pubDate;
			$newDate = date("d/m/Y", strtotime($originalDate));
		    $content = $node->children('content', 'http://purl.org/rss/1.0/modules/content/');
		    $html_string = $content->encoded;
		    $dom = new DOMDocument();
		    libxml_use_internal_errors(true);
		    $dom->loadHTML($html_string);
		    libxml_clear_errors();
		    $img = $dom->getElementsByTagName('img')->item(0)->getAttribute('src');
		    $smallTitle = substr($node->title, 0, 47);

		   	echo '<li>';	
		     	echo '<article>';	
			     	echo '<div class="post-photo" style="background-image: url('.utf8_decode(str_replace("http://","//",$img)).')">';
			     	echo '</div>';	
			     	echo '<h2>';	
			         	echo $smallTitle.'...';	
			         	echo '<small>'.$newDate.'</small>';	
			     	echo '</h2>';	
			     	echo '<p>';	
			         	echo $node->description;	
			     	echo '</p>';	
			     	echo '<a href="'.$node->link.'" target="_blank">Leia mais</a>';	
			 	echo '</article>';	
		 	echo '</li>';	
		  }	    	
	    }

  echo '</ul>';

}


?>


